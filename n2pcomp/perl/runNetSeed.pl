#!/usr/local/bin/perl -w


use strict;
use lib ($ARGV[0]); #This should point to where you keep NetSeed.pm
use NetSeed;


my $networkFile = $ARGV[2];

open (my $NetFH, "< $networkFile") or die "Can't find $networkFile: $!\n";
my $sep = $ARGV[1];
my $giantComponent = -1;
my $minimal = 0;

CalculateSeeds($NetFH,$sep,$giantComponent,$minimal);

# Grab the resulting data
my %Seeds = Seeds();
my %GroupedSeeds = GroupedSeeds();
my %NonSeeds = NonSeeds();
my %Ignored = IgnoredNodes();
my %Nodes = AllNodes();
my %Edges = AllEdges();
my $nElements = NumElements();

my $groupedLink = $ARGV[3];
foreach my $seed (keys %GroupedSeeds) { print "$seed\n"; }