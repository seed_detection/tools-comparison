#! /usr/bin/python3
# -*- coding: utf-8 -*-


import argparse
import xml.etree.ElementTree as etree
from xml.etree.ElementTree import Element, SubElement, Comment, ElementTree, tostring
import xml.dom.minidom as dom
from time import time
from precursor import __main__ as precursor_module

def add_species(list_element:Element, species:frozenset or list):
    """Add species to XML list element.

    Args:
        list_element (Element): XML list element.
        species (frozenset|list): List of species names.
    """
    xml_species_list = [
        Element('species', {'id': specie, 'comment': specie})
        for specie in species
    ]
    list_element.extend(xml_species_list)


def create_precursor_input(targets, seeds, forbidden_seeds, possible_seeds):
    """Precursor input creator.

    Args:
        targets ([type]): Targets compounds.
        seeds ([type]): Seeds compounds.
        forbidden_seeds ([type]): Forbidden compounds.
        possible_seeds ([type]): Possible seeds compounds.

    Returns:
        str: Precursor input content.
    """
    inputs = Element('inputs')
    target_compound_list = SubElement(inputs, 'target-compounds')
    precursor_compound_list = SubElement(inputs, 'precursor-compounds')
    forbidden_compound_list = SubElement(inputs, 'forbidden-compounds')
    add_species(target_compound_list, targets)
    add_species(precursor_compound_list, seeds)     # TODO: Merge seeds and possible seeds ?
    add_species(forbidden_compound_list, forbidden_seeds)
    # Pretty formatting
    pretty_xml = dom.parseString(tostring(inputs)).toprettyxml()
    return pretty_xml


def run_precursor(filename:str, targets:list, seeds:frozenset, forbidden_seeds:frozenset, possible_seeds:frozenset, dir:str, autoseeds:bool=True):
    """Run Precusor tool.

    Args:
        filename (str): Name of SBML network
        targets (list): Targets compounds.
        seeds (frozenset): Seeds compounds.
        forbidden_seeds (frozenset): Forbidden compounds.
        possible_seeds (frozenset): Possible seeds compounds.
        autoseeds (bool, optional): Autoseeds flag for Precursor program. Defaults to True.

    Returns:
        str: Execution content of Precursor tool.
    """
    autoseeds_flag = '--autoseeds' if autoseeds else ''
    in_filename = dir+"precursor_input.xml"
    with open(in_filename, 'w') as f:
        f.write(create_precursor_input(
            targets, seeds, forbidden_seeds, possible_seeds))
    args = {'net': filename,
            'inputs': in_filename,
            'autoseeds': autoseeds}
    exec_time = time()
    results = precursor_module.run_precursor(argparse.Namespace(**args))
    exec_time = time() - exec_time
    return results, exec_time
