#! /usr/bin/python3
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as etree
from xml.etree.ElementTree import Element, SubElement, Comment, ElementTree, tostring
import xml.dom.minidom as dom
from .sbml import *
from time import time
import subprocess
import os
import itertools

def create_netseed_input(filename:str, separator: str = ','):
    """NetSeed input creator

    Args:
        filename (str): Name of SBML network.
        separator (str, optional): Separator used in the input. Defaults to ','.

    Yields:
        str: NetSeed input content.
    """
    tree = etree.parse(filename)
    sbml = tree.getroot()
    model = get_model(sbml)
    reactions = get_listOfReactions(model)
    for e in reactions:
        tag = get_sbml_tag(e)
        if tag == "reaction":
            reactionId = e.attrib.get("id")
            reactants = get_listOfReactants(e)
            products = get_listOfProducts(e)
            reversible = e.attrib.get("reversible") == 'true'
            yield from make_reaction(reactionId, reactants, products, separator)
            if reversible:
                yield from make_reaction('rev_' + reactionId, products, reactants, separator)

def get_runNetSeed_path():
    """Get the 'runNetSeed.pl' file path

    Returns:
        str: 'runNetSeed.pl' file path
    """
    path_file=os.path.realpath(__file__)
    print(path_file)
    path_wanted='/'.join(path_file.split('/')[:-1])+'/perl'
    return path_wanted

def run_netseed(filename:str, dir:str, netseed_dir_path:str, sep:str=','):
    """Run NetSeed tool.

    Args:
        filename (str): Name of SBML network.
        sep (str, optional): Separator used in the input. Defaults to ','.

    Returns:
        str: Execution content of NetSeed tool.
    """
    in_filename = dir+"netseed_input.txt"
    run_netseed_path = get_runNetSeed_path() 
    with open(in_filename, 'w') as f:
        f.write('\n'.join(map(str, create_netseed_input(filename, separator=sep))))
    exec_time = time()
    output = subprocess.run(
        f"perl {run_netseed_path}/runNetSeed.pl {netseed_dir_path} {sep} {in_filename}",
        #f"perl n2pcomp/perl/runNetSeed.pl {netseed_dir_path} {sep} {in_filename}",

	shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding='utf-8')
    exec_time = time() - exec_time
    print("NETSEED OUTPUT: ",output.stdout)
    print("NETSEED ERR:  ",output.stderr)
    return output.stdout, output.stderr, exec_time


def enumeration_netseed_data_formatting(data: str) -> dict:
    """Format NetSeed output data for enumeration mode.

    Calculate the cartesian product.

    Args:
        data (str): Execution content of NetSeed tool.

    Returns:
        dict: Seeds results formatted for NetSeed tool.
    """ 
    if data is None: return None
    data_list = list()
    solutions = dict()
    solution_nb = 1
    for line in data.split('\n'):
        if line == '': continue
        data_list.append(line.split(','))
    formatted_data = (list(_) for _ in itertools.product(*data_list))
    for sol in formatted_data:
        solutions[solution_nb] = sol
        solution_nb += 1
    return solutions    

def non_enumeration_netseed_data_formatting(data: str) -> dict:
    """Format NetSeed output data for non enumeration mode.

    Calculate the cartesian product.

    Args:
        data (str): Execution content of NetSeed tool.

    Returns:
        dict: Seeds results formatted for NetSeed tool.
    """ 
    if data is None: return None
    data_list = list()
    one_solution = list()
    union = list()
    intersection = list()
    for line in data.split('\n'):
        if line == '': continue
        data_list.append(line.split(','))
    for l in data_list:
        one_solution.append(l[0])
        if len(l) == 1:
            intersection.append(l[0])
        union.extend(l)
    return {'one_solution': one_solution, 'union': union, 'intersection':  intersection}
