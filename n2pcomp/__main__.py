#! /usr/bin/python3
# -*- coding: utf-8 -*-

import os
import argparse
import re
import logging
from configparser import ConfigParser, NoSectionError
from json import dump
from yaml import load, FullLoader, YAMLError
from shutil import copy2
from time import strftime, time

from . import __version__
from .netseed import run_netseed, enumeration_netseed_data_formatting, non_enumeration_netseed_data_formatting
from .precursor import run_precursor, create_precursor_input
from .predator import run_predator
from .sbml import *

DESCRIPTION='Comparator of seed detection programs'

def existant_file(filepath: str) -> str:
    """Argparse type, raising an error if given file does not exists.

    Args:
        filepath (str): File path.

    Raises:
        argparse.ArgumentTypeError: File does not exist.
        argparse.ArgumentTypeError: File have not good format.

    Returns:
        str: File path.
    """
    if not os.path.exists(filepath):
        raise argparse.ArgumentTypeError(
            "file {} doesn't exists".format(filepath))
    ext = os.path.splitext(filepath)[1]
    if ext not in {'.xml', '.sbml'}:
        raise argparse.ArgumentTypeError(
            "file {} have not good format".format(filepath))
    return filepath


def cli_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        "n2pcomp",
        description=DESCRIPTION
    )
    # positionnals
    parser.add_argument('network', type=existant_file,
                        help="SBML or ASP file containing the graph data.")

    # parser.add_argument('--save-generated-data', '-sv', action='store_true',
    #                     help="Save generated data")
    parser.add_argument('--output', '-o', type=str, default=None,
                        help="Output directory name.")
    parser.add_argument('--verbose', '-v', action='store_true',
                        help="Print every process steps.")
    parser.add_argument('--conf-file', '-c', type=str, default=None,
                        help="Configuration file path.")
    parser.add_argument('--netseed', action='store_true',
                        help="Run NetSeed and quit.")
    parser.add_argument('--precursor', action='store_true',
                        help="Run Precursor and quit.")    
    
    # meta
    parser.add_argument('--version', '-V', action='version', version=__version__)
    # arguments
    parser.add_argument('--targets', '-t', nargs='*', type=str, default=[],
                        help="targets to activate in the graph")
    parser.add_argument('--seeds', '-s', nargs='*', type=str, default=[],
                        help="seeds already activated in the graph")
    parser.add_argument('--possible-seeds', '-ps', nargs='*', type=str, default=[],
                        help="set of compounds in which seed selection will be performed (greedy mode only)")
    parser.add_argument('--forbidden-seeds', '-fs', nargs='*', type=str, default=[],
                        help="metabolites that cannot be seed in the graph")
    parser.add_argument('--targets-are-forbidden', '-taf', action='store_true',
                        help="Targets are added to forbidden seeds")

    return parser


def parse_args(args:iter=None) -> dict:
    """Arguments parser

    Args:
        args (iter, optional): Command line arguments. Defaults to None.

    Returns:
        dict: Arguments
    """
    return cli_parser().parse_args(args)


def parse_conf_file(cfg_filename:str) -> dict:
    """Configuration file parser

    Args:
        cfg_filename (str): Name of the configuration file.

    Returns:
        dict: Options of each tools.
    """
    try:
        with open(cfg_filename, "r") as yamlfile:
            data = load(yamlfile, Loader=FullLoader)
    except FileNotFoundError:
        raise FileNotFoundError(f"Configuration file '{cfg_filename}' not found.")
    except YAMLError:
        raise YAMLError("Configuration file format not correct.")
    return data



def create_compounds_set(arg:str) -> frozenset:
    """Transform compounds (of the command line or file) into a set.

    Args:
        arg (str): Name of compounds and/or filenames which contains compounds.

    Returns:
        frozenset: Set of compound names.
    """
    compounds = list()
    for elt in arg:
        if os.path.isfile(elt):
            with open(elt, 'r') as f:
                lines = f.readlines()
                for line in lines:
                    compounds.append(line.rstrip('\r\n'))
        else:
            compounds.append(elt)
    return frozenset(compounds)

if __name__ == '__main__':
    # Arguments parsing
    args = parse_args()
    # Treat verbose mode
    _print = print if args.verbose else lambda *_, **__: None
    
    _print(">>> START.")

    # Treat paths tools
    cfg_filename  = args.conf_file if args.conf_file else 'config.yaml'
    cfg = parse_conf_file(cfg_filename)
    
    simple_run = not args.netseed and not args.precursor

    if simple_run:
        # Output directories storage creations
        _print("\tStorage directory creation...", end='\t')
        dir =  strftime("%Y_%m_%d-%I_%M_%S_%p")+'/'     # Format : "AAAA_MM_DD-HH_MM_SS_{AM|PM}"
        if args.output:
            dir = args.output+'/'
        try:
            os.mkdir(dir)
        except FileExistsError:
            print(f"Directory '{dir}' already exists.")
            quit()
        
        # Creation a folder for tools inputs
        os.mkdir(dir+'inputs')
        _print("Done.")     # End of 'Storage directory creation...' step

        # Logger creation
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(levelname)s: %(message)s')
        file_handler = logging.FileHandler(dir+'n2pcomp.log', 'w+')
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        
        # Make a copy of configuration file    
        _print("\tCopy of configuration file...", end='\t')
        copy2(cfg_filename, dir)
        _print("Done.")

        # Debug log
        logger.info(vars(args))
    
    # Define target, seeds, forbidden compounds, ... 
    _print("\tDefine target, seeds, forbidden compounds...", end='\t')
    targets = create_compounds_set(args.targets)
    seeds = create_compounds_set(args.seeds)
    forbidden_seeds = create_compounds_set(args.forbidden_seeds)
    possible_seeds = create_compounds_set(args.possible_seeds)


    precursor_targets = targets

    # Search to activate everything, 
    # so add all compounds from SBML network to the targets list (needed for precursor)
    if len(targets) == 0:   
        if simple_run: logger.info("No target given: activate everything.")
        tree = etree.parse(args.network)
        sbml = tree.getroot()
        model = get_model(sbml)
        precursor_targets = [elt.get('id') for elt in get_listOfSpecies(model)]
    _print("Done.") # End of 'Define target, seeds, forbidden compounds...' step

    # Run only NetSeed if it is asked
    if args.netseed:
        out_, err, _ = run_netseed(args.network, dir='/tmp/', netseed_dir_path=cfg["netseed"]["path"], sep=cfg['netseed']['separator'])
        print(out_)
        print(err)
        quit()

    # Run only Precursor if it is asked
    if args.precursor:
        run_precursor(args.network, precursor_targets, seeds, forbidden_seeds,
                                                     possible_seeds, dir='/tmp/', 
                                                     autoseeds=cfg['precursor']['autoseeds'])
        quit()


    
    # Tools run
    results = dict()
    if cfg['netseed']['run']:
        _print("\tRun NetSeed...", end='\t')
        netseed_data, netseed_error, netseed_time = run_netseed(args.network, dir=dir+'inputs/', netseed_dir_path=cfg["netseed"]["path"], sep=cfg['netseed']['separator'])
        if netseed_error:
            logger.critical(netseed_error)
        if not netseed_data:
            logger.info("No data produced from NetSeed run.")
        netseed_time = round(netseed_time, 3)
        if cfg['netseed']['enumeration']:
            exec_time = time()
            formatted_data = enumeration_netseed_data_formatting(netseed_data)
            exec_time = time() - exec_time
            results["netseed"] = {
                "solutions": formatted_data,
                "time": netseed_time,
                "time_all": round(exec_time+netseed_time, 3),
                "unproducible-targets": []
            }
        else:
            formatted_data = non_enumeration_netseed_data_formatting(netseed_data)
            results["netseed"] = {
                "time": netseed_time,
                "unproducible-targets": []
            }
            results["netseed"].update(formatted_data)
        _print(f"Done in {str(round(netseed_time, 3))}s.")
    
    if cfg['precursor']['run']:
        _print("\tRun Precursor...", end='\t')
        precursor_data, precursor_time = run_precursor(args.network, precursor_targets, seeds, forbidden_seeds,
                                                        possible_seeds, dir=dir+'inputs/', 
                                                        autoseeds=cfg['precursor']['autoseeds'])
        if not precursor_data:
            logger.info("No data produced from Precursor run.")
        precursor_time = round(precursor_time, 3)
        results["precursor"] = precursor_data
        results["precursor"]["time"] = precursor_time
        _print(f"Done in {str(precursor_time)}s.")


    if cfg['predator']['run'] or cfg['predator']['netseed-asp']:
        # Prepare options
        predator_opt = cfg["predator"]
        predator_opt['targets'] = targets 
        predator_opt['seeds'] = seeds
        predator_opt['forbidden-seeds'] = forbidden_seeds
        predator_opt['possible-seeds'] = possible_seeds
        predator_opt['targets-are-forbidden'] = args.targets_are_forbidden

    if cfg['predator']['run']:
        _print("\tRun Predator...", end='\t')        
        predator_opt['netseed-asp'] = False
        predator_data, predator_time = run_predator(args.network, predator_opt)     
        if not predator_data:
            logger.info("No data produced from Predator run.")
        predator_time = round(predator_time, 3)    
        results["predator"] = {
            "solutions": predator_data,
            "time": predator_time,
            "unproducible-targets": []
        } 
        _print(f"Done in {str(predator_time)}s.")
    
    if cfg['predator']['netseed-asp']:
        _print("\tRun NetSeed-ASP...", end='\t')
        netseed_asp_data, netseed_asp_time = run_predator(args.network, predator_opt)     
        if not netseed_asp_data:
            logger.info("No data produced from NetSeed-ASP run.")
        netseed_asp_time = round(netseed_asp_time, 3)    
        results["netseed_asp"] = {
            "solutions": netseed_asp_data,
            "time": netseed_asp_time,
            "unproducible-targets": []
        } 
        _print(f"Done in {str(netseed_asp_time)}s.")

        
    # Write results into JSON file
    with open(dir+"results.json", 'w') as f:
        dump(results, f, sort_keys=True, indent="\t")

    _print(">>> END.")
    