#! /usr/bin/python3
# -*- coding: utf-8 -*-

from time import time

from predator import __main__ as predator_module

def run_predator(filename:str, args_dict:dict):
    """Run Predator tool.

    Args:
        filename (str): Name of SBML network.
        args_dict (dict): Command line arguments.

    Returns:
        str: Execution content of Predator tool.
    """
    args_dict['infile'] = filename
    formatted_args_dict = dict()
    for key in args_dict.keys():
        formatted_args_dict[key.replace('-', '_')] = args_dict[key]
    for arg in ['targets_file', 'seeds_file', 'forbidden_seeds_file', 'possible_seeds_file']:
        formatted_args_dict[arg] = None
    exec_time = time()
    results = predator_module.run_predator(formatted_args_dict)
    exec_time = time() - exec_time
    return results, exec_time