# N2PComp : Comparator of seed detection tools

The aim of this software is to run the 3 following tools in one execution, in order to compare their results and see difference easily.
For that, the program creates inputs required for NetSeed and Precursor, based on information given in command line.

Tools compared : 
* `Netseed` (Perl module) - https://elbo.gs.washington.edu/software_netseed.html
* `Precursor` - https://gitlab.inria.fr/seed_detection/precursor 
* `Predator` - https://gitlab.inria.fr/seed_detection/predator 

## Install

```
pip install .
```

## Requirements

Requires Python >= 3.6. 

**Warning**: No warranty using Python 3.9 

Required packages:
* `clyngor`>=0.3.18 or `clyngor-with-clingo`>=5.3.0
* `pyyaml`>=5.4.1
* `predator` (see [https://gitlab.inria.fr/seed_detection/predator](https://gitlab.inria.fr/seed_detection/predator))
* `precursor` (see [https://gitlab.inria.fr/seed_detection/precursor](https://gitlab.inria.fr/seed_detection/precursor))

NetSeed Perl module is also required (see [http://borensteinlab.com/software_netseed.html](http://borensteinlab.com/software_netseed.html)). The folder where the module is stored is required in the configuration file (see `config.yaml` file for more information).

## Configuration

To work, programs needs a configuration file which contains each tool options. By default, program takes the configuration file present in the current folder (with name `config.yaml`), but give a path to the configuration file wanted is also possible (see `--conf-file`/`-c` argument). To have an idea of the configuration file format see `config.yaml` file.

## Execution
At each execution of program, a log file is created. Moreover, input files for NetSeed and Precursor are also generated. All of these files are stored in a folder with "AAAA_MM_DD-HH_MM_SS_{AM|PM}" format as name. But, users can give another folder name thanks to the `--export`/`-e` argument.

Verbose functionality is available (see `--verbose`/`-v` argument).

## Usage
```
usage: n2pcomp [-h] [--output OUTPUT] [--verbose] [--conf-file CONF_FILE]
               [--netseed] [--precursor] [--version]
               [--targets [TARGETS [TARGETS ...]]]
               [--seeds [SEEDS [SEEDS ...]]]
               [--possible-seeds [POSSIBLE_SEEDS [POSSIBLE_SEEDS ...]]]
               [--forbidden-seeds [FORBIDDEN_SEEDS [FORBIDDEN_SEEDS ...]]]
               [--targets-are-forbidden]
               network

Comparator of seed detection programs

positional arguments:
  network               SBML or ASP file containing the graph data.

optional arguments:
  -h, --help            show this help message and exit
  --output OUTPUT, -o OUTPUT
                        Output directory name.
  --verbose, -v         Print every process steps.
  --conf-file CONF_FILE, -c CONF_FILE
                        Configuration file path.
  --netseed             Run NetSeed and quit.
  --precursor           Run Precursor and quit.
  --version, -V         show program's version number and exit
  --targets [TARGETS [TARGETS ...]], -t [TARGETS [TARGETS ...]]
                        targets to activate in the graph
  --seeds [SEEDS [SEEDS ...]], -s [SEEDS [SEEDS ...]]
                        seeds already activated in the graph
  --possible-seeds [POSSIBLE_SEEDS [POSSIBLE_SEEDS ...]], -ps [POSSIBLE_SEEDS [POSSIBLE_SEEDS ...]]
                        set of compounds in which seed selection will be performed (not used)
  --forbidden-seeds [FORBIDDEN_SEEDS [FORBIDDEN_SEEDS ...]], -fs [FORBIDDEN_SEEDS [FORBIDDEN_SEEDS ...]]
                        metabolites that cannot be seed in the graph
  --targets-are-forbidden, -taf
                        Targets are added to forbidden seeds
```

## Results
After execution, results in JSON format are generated. The file is formated like that : 
```json
{
	"precursor": {
		"solutions": {
			"1": ["D"],
			"2": ["B","A"]
		},
		"time": 1.257,
		"unproducible-targets": []
	},
	"predator": {
		"solutions": {
			"1": ["B","A"],
			"2": ["B","D"],
			"3": ["B","E"]
		},
		"time": 0.832,
		"unproducible-targets": []
	}
}
```
For each tool, solutions are visible in a JSON format. The key is the number of the solution and the value is a list with the seeds. The duration of execution (in seconds) is also available.

## Run examples

- Activating all metabolites:
    ```bash
    python -m n2pcomp data.sbml
    ```
- Activate some targets:
    ```bash
    python -m n2pcomp data.sbml -t protein_1 protein_2
    ```
- Give some seeds:
    ```bash
    python -m n2pcomp data.sbml -s seed_1 seed_2 seed_3
    ```
- Give directory name for store results:
    ```bash
    python -m n2pcomp data.sbml --output output_folder
    ```
- Give conf file path:
    ```bash
    python -m n2pcomp data.sbml -c path/to/conf_file.yaml
    ```
- Get results of precursor tool and exit:
    ```bash
    python -m n2pcomp data.sbml --precursor
    ```
