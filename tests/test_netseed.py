import n2pcomp
from n2pcomp.netseed import create_netseed_input, run_netseed
from n2pcomp.sbml import make_reaction
import argparse

def test_create_netseed_input():        
    expected = (x for x in ['a,b', 'c,d', 'd,c','e,c', 'e,f'])
    produced = create_netseed_input('tests/data/netseed_input.xml')
    assert set(expected) == set(produced)

def test_run_netseed():
    expected = ['a', 'e']
    produced, _, _ = run_netseed('tests/data/netseed_input.xml', '/tmp/', '../NetSeedPerl/', sep=',')
    produced = produced.split('\n')
    produced = list(filter(None, produced))
    assert set(produced) == set(expected)

def test_make_reaction():
    # TODO
    pass
        