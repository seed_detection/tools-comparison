import n2pcomp
from n2pcomp.precursor import create_precursor_input, run_precursor


def test_create_precursor_input():
	
	expected = """<?xml version="1.0" ?>
<inputs>
	<target-compounds>
		<species id="a" comment="a"/>
	</target-compounds>
	<precursor-compounds>
		<species id="b" comment="b"/>
		<species id="c" comment="c"/>
	</precursor-compounds>
	<forbidden-compounds>
		<species id="a" comment="a"/>
	</forbidden-compounds>
</inputs>
"""
	produced = create_precursor_input(targets=['a'], seeds=['b', 'c'], forbidden_seeds=['a'], possible_seeds=[])
	assert expected == produced

def test_run_precursor():	
	expected = ['e', 'a', 'b']
	produced, _ = run_precursor("networks/orgA.xml", targets=frozenset({'a', 'b', 'c', 'd', 'e', 'f'}), 
								seeds=frozenset(), forbidden_seeds=frozenset(),possible_seeds=frozenset(), 
								dir='/tmp/', autoseeds=True)
	assert set(produced['solutions'][1]) == set(expected)
