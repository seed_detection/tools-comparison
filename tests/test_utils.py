from pytest import raises
import n2pcomp
from n2pcomp.__main__ import existant_file, parse_conf_file, create_compounds_set
import argparse

def test_existant_file():
    # Test existant file when file exist and have good extension
    assert existant_file('networks/orgA.xml') == 'networks/orgA.xml'
    # Test existant file when file exist and have bad extension
    raises(argparse.ArgumentTypeError, existant_file, 'README.md')
    # # Test absent file
    raises(argparse.ArgumentTypeError, existant_file, 'a.sbml')


def test_parse_conf_file():
    # Test existant file
    assert parse_conf_file('tests/data/config_file.yaml') == {'test':True}
    # Test absent file
    raises(FileNotFoundError, parse_conf_file, 'a.test')

def test_create_compounds_set():
    assert create_compounds_set(['a']) == frozenset('a')
    assert create_compounds_set(['a', 'b']) == frozenset({'a', 'b'})
    assert create_compounds_set([]) == frozenset()
    assert create_compounds_set(['compound']) == frozenset({'compound'})
    assert create_compounds_set(['tests/data/compounds_set.txt']) == frozenset({'x'})
    assert create_compounds_set(['tests/data/compounds_set.txt', 'a', 'v']) == frozenset({'x', 'a', 'v'})